// Server Variable
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT || 4000;
const app = express();
const projectRoutes = require ('./routes/projectRoutes.js')


// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routes
app.use('/api/projects', projectRoutes);



// Database connection 
mongoose.connect(`${process.env.MONGODB_CONNECTION_STRING}`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.on('error', () => console.log("Can't connect to database"));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));



app.listen(process.env.PORT || port, () => {
	console.log(`My Portfolio API is now running at localhost: ${process.env.PORT || port}`);
})

module.exports = app;