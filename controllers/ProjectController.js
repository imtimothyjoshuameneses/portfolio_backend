const Project = require('../models/Project.js');

// Add/Create single Project 
module.exports.addProject = (request, response) => {
    let new_project = new Project({
        name: request.body.name,
        description: request.body.description,
        gitlabLink: request.body.gitlabLink,
        deployLink: request.body.deployLink,
        image: request.body.image
    });

    return new_project.save().then((saved_project, error) => {
        if(error) {
            return response.send({success: false, message: error.message});
        }
        return response.send({success: true, message: 'Project added successfully!'});
    }).catch(error => console.log(error));
}


// Retrieve all projects
module.exports.getAllProjects = (request, response) => {
    return Project.find({}).then(result => {
        return response.send(result);
    })
}

// Retrieve all Active Projects
module.exports.getAllActiveProjects = (request, response) => {
    return Project.find({isActive: true}).then(result => {
        return response.send(result)
    })
}

// Retrieve Specific Project
module.exports.getSingleProject = (request, response) => {
    return Project.findById(request.params.id).then(result => {
        return response.send(result);
    })
}

// Update Project
module.exports.updateProject = (request, response) => {
    let update_project_details = {
        name: request.body.name,
        description: request.body.description,
        gitlabLink: request.body.gitlabLink,
        deployLink: request.body.deployLink,
        image: request.body.image
    };

    return Project.findByIdAndUpdate(request.params.id, update_project_details).then((updated_project, error) => {
        if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Project has been updated Successfully'
		})
    }).catch(error => console.log(error))
}

// Archive Project
module.exports.archiveProject = (request, response) => {
	return Project.findByIdAndUpdate(request.params.id, {isActive: 'false'},{new: true}).then((project_archived, error) => {

		if(error) {
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Project has been archived'
		})
	}).catch(error => console.log(error));
}


// Activate Project
module.exports.activateProject = (request, response) => {
	return Project.findByIdAndUpdate(request.params.id, {isActive: "true"},{new: true}).then((project_activated, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Project has been activated'
		})
	}).catch(error => console.log(error));
}