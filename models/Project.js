const mongoose = require('mongoose');

// Schema
const project_schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Project Name is Required!']
    },
    description : {
        type : String,
        required : [true, 'Project Description is Required']
    },
    gitlabLink : {
        type : String,
        required : [true, 'Gitlab link is required!']
    },
    deployLink : {
        type: String,
        required : [true, 'Deploy link is required']
    },
    isActive : {
        type : Boolean,
        default : true
    },
    image : {
        type: String,
        required: [true, "Product Image is required!"]
    }
});

module.exports = mongoose.model("Project", project_schema);

