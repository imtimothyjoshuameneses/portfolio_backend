const express = require('express');
const router = express.Router();
const ProjectController = require('../controllers/ProjectController.js')

// Add new Project 
router.post('/', (request, response) => {
    ProjectController.addProject(request, response)
});

// Retrieve all Projects
router.get('/all', (request, response) => {
    ProjectController.getAllProjects(request, response)
})

// Retrieve all active Projects
router.get('/', (request, response) => {
    ProjectController.getAllActiveProjects(request, response)
})

// Retrieve Specific Project
router.get('/:id',  (request, response) => {
    ProjectController.getSingleProject(request, response);
})

// Update Specific Project 
router.put('/:id', (request, response) => {
    ProjectController.updateProject(request, response);
})

// Archive Project
router.put('/:id/archive', (request, response) => {
	ProjectController.archiveProject(request, response);

})

// Activate project 
router.put('/:id/activate', (request, response) => {
	ProjectController.activateProject(request, response);
})

module.exports = router;